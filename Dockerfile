FROM ubuntu:20.04 AS builder
RUN sed -i -e '/^# deb-src .* main/s/^# //' /etc/apt/sources.list && apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get build-dep -y mutt
RUN apt-get install -y git
WORKDIR /build
RUN git clone https://gitlab.com/muttmua/mutt.git
WORKDIR /build/mutt
RUN ./prepare --prefix=/usr/local/stow/mutt && make && make install

FROM ubuntu:20.04
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
        mutt \
        stow \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/stow/mutt /usr/local/stow/mutt
RUN stow --dir /usr/local/stow mutt
